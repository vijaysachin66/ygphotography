$('.home-slider').slick({
  infinite: true,
  arrows: true,
  dots: false,
  autoplay: false,
  speed: 1000,
  slidesToShow: 1,
  slidesToScroll: 1,
  nextArrow:
    '<div class="slick-right"><img src="assets/images/right.png"></div>',
  prevArrow: '<div class="slick-left"><img src="assets/images/left.png"></div>'
});

// Testimonail
$('.testimonial-cards').slick({
  infinite: true,
  arrows: false,
  dots: true,
  autoplay: false,
  speed: 1000,
  slidesToShow: 3,
  slidesToScroll: 1,
  centerMode: true,
  centerPadding: '1px',
  responsive: [
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});
